<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_posts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id')->nullable();
            $table->foreignId('job_category_id')->nullable();
            $table->string('title')->nullable();
            $table->integer('no_of_positions')->nullable();
            $table->string('job_level')->nullable();
            $table->integer('salary')->nullable();
            $table->foreignId('job_location_id')->nullable();
            $table->string('qualification')->nullable();
            $table->string('experience')->nullable();
            $table->mediumText('description')->nullable();
            $table->date('expiry_date')->nullable();
            $table->boolean('status')->nullable();
            $table->timestamps();

            $table->foreign('company_id')->references('id')->on('companies')->onDelete('set null');
            $table->foreign('job_category_id')->references('id')->on('job_categories')->onDelete('set null');
            $table->foreign('job_location_id')->references('id')->on('job_locations')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_posts');
    }
}
