<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobPostApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_post_applications', function (Blueprint $table) {
            $table->id();
            $table->foreignId('job_seeker_id')->nullable();
            $table->foreignId('job_post_id')->nullable();
            $table->mediumText('cover_letter');
            $table->timestamps();

            $table->foreign('job_seeker_id')->references('id')->on('job_seekers')->onDelete('set null');
            $table->foreign('job_post_id')->references('id')->on('job_posts')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_post_applications');
    }
}
