<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Aadil Kansakar',
            'username' => 'aadilkansakar',
            'phone' => '123456789',
            'email' => 'aadilkansakar@gmail.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        ]);

        $user->assignRole(Role::findByName('SuperAdmin'));

        $user = User::create([
            'name' => 'James Milner',
            'username' => 'jamesmilner',
            'email' => 'jamesmilner@example.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        ]);

        $user->assignRole(Role::findByName('Viewer'));

        $user = User::create([
            'name' => 'Ekbana',
            'username' => 'ekbana',
            'email' => 'ekbana@example.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        ]);

        $user->assignRole(Role::findByName('Manager'));

        $user = User::create([
            'name' => 'Samina Tuladhar',
            'username' => 'saminatuladhar',
            'email' => 'samina@example.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        ]);

        $user->assignRole(Role::findByName('Administrator'));

        $users = User::factory()->count(6)->create();

        foreach ($users as $user) {
            $user->assignRole(Role::findByName('Viewer'));
        }
        
        $users = User::factory()->count(4)->create();

        foreach ($users as $user) {
            $user->assignRole(Role::findByName('Manager'));
        }

        $users = User::factory()->count(1)->create();

        foreach ($users as $user) {
            $user->assignRole(Role::findByName('Administrator'));
        }

        $users = User::factory()->count(4)->create();

        foreach ($users as $user) {
            $user->assignRole(Role::findByName('Creator'));
        }

        $users = User::factory()->count(4)->create();

        foreach ($users as $user) {
            $user->assignRole(Role::findByName('Editor'));
        }

        $users = User::factory()->count(4)->create();

        foreach ($users as $user) {
            $user->assignRole(Role::findByName('Deleter'));
        }
    }
}
