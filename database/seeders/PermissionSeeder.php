<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            'user-password-reset',
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'jobcategory-list',
            'jobcategory-create',
            'jobcategory-edit',
            'jobcategory-delete',
            'jobseeker-list',
            'jobseeker-create',
            'jobseeker-edit',
            'jobseeker-delete',
        ];
      
        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }
    }
}
