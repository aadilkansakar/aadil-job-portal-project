<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create(['name' => 'SuperAdmin']);
        $role->givePermissionTo(Permission::all());

        $role = Role::create(['name' => 'Administrator']);
        $role->givePermissionTo([
            'user-list',
            'user-create',
            'user-edit',
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'jobcategory-list',
            'jobcategory-create',
            'jobcategory-edit',
            'jobcategory-delete',
            'jobseeker-list',
            'jobseeker-create',
            'jobseeker-edit',
            'jobseeker-delete',
        ]);

        $role = Role::create(['name' => 'Manager']);
        $role->givePermissionTo([
            'user-list',
            'role-list',
            'jobcategory-list',
            'jobcategory-create',
            'jobcategory-edit',
            'jobcategory-delete',
            'jobseeker-list',
        ]);

        $role = Role::create(['name' => 'Viewer']);
        $role->givePermissionTo([
            'user-list',
            'role-list',
            'jobcategory-list',
            'jobseeker-list',
        ]);

        $role = Role::create(['name' => 'Creator']);
        $role->givePermissionTo([
            'user-create',
            'role-create',
            'jobcategory-create',
            'jobseeker-create',
        ]);

        $role = Role::create(['name' => 'Editor']);
        $role->givePermissionTo([
            'user-edit',
            'role-edit',
            'jobcategory-edit',
            'jobseeker-edit',
        ]);

        $role = Role::create(['name' => 'Deleter']);
        $role->givePermissionTo([
            'user-delete',
            'role-delete',
            'jobcategory-delete',
            'jobseeker-delete',
        ]);
    }
}
