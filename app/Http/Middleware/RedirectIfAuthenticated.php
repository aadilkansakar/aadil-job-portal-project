<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @param  string|null  ...$guards
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                if ($guard == 'jobseeker') {
                    return redirect()->route('jobseeker.profile')->with('error', 'Cannot Access Page!!');
                }
                elseif ($guard == 'company') {
                    return redirect()->route('company.home')->with('error', 'Cannot Access Page!!');
                }
                else {
                    return redirect()->route('admin.home')->with('error', 'Cannot Access Page!!');
                }
            }
        }

        return $next($request);
    }
}
