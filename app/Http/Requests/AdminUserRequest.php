<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = [
            'name' => 'required|string|max:255',
            'username' => 'nullable|string|max:255',
            'email' => 'required|string|email|max:255',            
            'phone' => 'nullable|string|max:255',
            'roles' => 'required',
        ];

        if (request()->isMethod('POST')) {
            $data = array_merge($data + ['password' => 'required|string|max:255',]);
            return $data;
        }
        elseif (request()->isMethod('PUT')) {
            $data = array_merge($data + ['password' => 'string|max:255',]);
            return $data;
        }    
    }
}
