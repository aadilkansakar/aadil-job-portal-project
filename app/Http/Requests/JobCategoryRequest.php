<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JobCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = [
            'name' => 'required|string|max:255',     
            'status' => 'required|boolean',
        ];

        if (request()->isMethod('POST')) {
            $data = array_merge($data + ['slug' => 'required|string|max:255|unique:job_categories,slug']);
        }
        elseif (request()->isMethod('PUT')) {
            $data = array_merge($data + ['slug' => 'required|string|max:255']);
        }

        return $data;
    }
}
