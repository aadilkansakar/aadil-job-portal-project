<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd(request()->permission);
        if (request()->isMethod('POST')) {
            return [
                'name' => 'required|unique:roles,name',
                'permission' => 'required|array|min:1',
            ];
        }
        elseif (request()->isMethod('PUT')) {
            return [
                'name' => 'required',
                'permission' => 'required|array|min:1',
            ];
        }
    }
}
