<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JobSeekerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->isMethod('POST')) {
            $data = [
                'name' => 'required|string|max:255',
                'phone' => 'required',
                'username' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:job_seekers',
                'password' => 'required|string|min:8|confirmed',
                'password_confirmation' => 'required',
            ];
        }
        elseif (request()->isMethod('PUT')) {
            $data = [
                'name' => 'required|string|max:255',
                'phone' => 'required',
                'username' => 'nullable|string|max:255',
                'email' => 'required|string|email|max:255|unique:job_seekers,email,'.$this->user()->id,
                'date_of_birth' => 'nullable|date',
                'gender' => 'nullable',
                'profile_picture' => 'nullable|image|mimes:png,jpg|max:2048',
                'bio' => 'nullable|string',
                'job_category_id' => 'nullable',
            ];
        }

        return $data;
    }
}
