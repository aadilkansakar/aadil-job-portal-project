<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\JobCategoryRequest;
use App\Models\JobCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class JobCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:jobcategory-list|jobcategory-create|jobcategory-edit|jobcategory-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:jobcategory-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:jobcategory-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:jobcategory-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $jobcategories = $this->search($request);

        return view('admin.jobcategory.index', compact('jobcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.jobcategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobCategoryRequest $request)
    {
        JobCategory::create($request->validated());

        return redirect()->route('admin.jobcategories.index')->with('success', 'Job Category added!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(JobCategory $jobcategory)
    {
        return view('admin.jobcategory.edit', compact('jobcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(JobCategoryRequest $request, JobCategory $jobcategory)
    {
        $jobcategory->update($request->validated());

        return redirect()->route('admin.jobcategories.index')->with('status', 'Job Category Updated!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(JobCategory $jobcategory)
    {
        $jobcategory->delete();

        return redirect()->route('admin.jobcategories.index')->with('error', 'Job Category Deleted!!!');
    }

    public function checkSlug(Request $request)
    {
        // MAKE SLUG FROM NAME USING JQUERY

        $slug = Str::slug($request->name);

        return response()->json(['slug' => $slug]);
    }

    public function search($request)
    {
        $keyword = $request->query('keyword');
        $queryStatus = $request->query('status');

        // SEARCH USING KEYWORD AND STATUS

        if ($queryStatus === NULL || ($queryStatus != 1 && $queryStatus != 0)) {
            $queryStatus = 'all';
        }

        if (empty($keyword) && ($queryStatus == "all")) {
            $jobcategories = JobCategory::get();
        }
        elseif ($queryStatus == 'all') {
            $jobcategories = JobCategory::where('name', 'LIKE', "%$keyword%")
                ->orWhere('slug', 'LIKE', "%$keyword%")
                ->get();
        }
        elseif (empty($keyword)) {
            $jobcategories = JobCategory::where('status', '=', $queryStatus)->get();
        }
        elseif (!empty($keyword) && ($queryStatus !== "all")) {
            $jobcategories = JobCategory::where([
                    ['status', '=', $queryStatus],
                    ['name', 'LIKE', "%$keyword%"]
                ])->orWhere([
                    ['status', '=', $queryStatus],
                    ['slug', 'LIKE', "%$keyword%"]
                ])->get();
        }

        return $jobcategories;
    }
}
