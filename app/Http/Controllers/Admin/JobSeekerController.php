<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\JobSeekerRequest;
use App\Models\JobSeeker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class JobSeekerController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:jobseeker-list|jobseeker-create|jobseeker-edit|jobseeker-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:jobseeker-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:jobseeker-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:jobseeker-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->query('keyword');

        $jobseekers = $this->search($keyword);

        return view('admin.jobseekers.index', compact('jobseekers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.jobseekers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobSeekerRequest $request)
    {
        $JobSeeker = JobSeeker::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        return redirect()->route('admin.jobseekers.index')->with('success', $JobSeeker->name.' Job Seeker created successfully!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(JobSeeker $jobseeker)
    {
        return view('admin.jobseekers.edit', compact('jobseeker'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(JobSeekerRequest $request, JobSeeker $jobseeker)
    {
        $jobseeker->update($request->validated());

        return redirect()->route('admin.jobseekers.index')->with('status', $jobseeker->name.' has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(JobSeeker $jobseeker)
    {
        $jobseeker->delete();

        return redirect()->route('admin.jobseekers.index')->with('error', 'Job Seeker has been deleted!!!');
    }

    // SEARCH FUNCTION FOR QUERYING
    public function search($keyword)
    {
        $jobseeker = JobSeeker::where('name', 'LIKE', "%$keyword%")
            ->orWhere('username', 'LIKE', "%$keyword%")
            ->orWhere('email', 'LIKE', "%$keyword%")
            ->get();

        return $jobseeker;
    }
}
