<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminUserPasswordRequest;
use App\Http\Requests\AdminUserRequest;
use Spatie\Permission\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:user-list|user-create|user-edit|user-delete|user-password-reset', ['only' => ['index', 'show']]);
        $this->middleware('permission:user-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
        $this->middleware('permission:user-password-reset', ['only' => ['showResetForm', 'resetPassword']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, User $user)
    {
        $keyword = $request->query('keyword');
        $queryRole = $request->query('role');
        
        // SEARCH USING KEYWORD AND ROLE
        $users = $this->search($keyword, $queryRole);

        $allRoles = Role::all();

        return view('admin.adminuser.index', compact('users', 'allRoles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();

        return view('admin.adminuser.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminUserRequest $request)
    {
        $user = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => bcrypt('password'),
        ]);

        $user->assignRole($request->input('roles'));

        return redirect()->route('admin.users.index')->with('success', 'User '.$user->name.' has been created!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles = Role::all();
        $userRole = $user->roles->first();

        return view('admin.adminuser.edit', compact('user', 'roles', 'userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  User $user
     * @return \Illuminate\Http\Response
     */
    public function update(AdminUserRequest $request, User $user)
    {
        $user->update($request->validated());
        
        // MODEL ROLE DELETING FROM TABLE BEFORE UPDATING WITH NEW VALUE

        DB::table('model_has_roles')->where('model_id', $user->id)->delete();

        $user->assignRole($request->input('roles'));

        return redirect()->route('admin.users.index')->with('status', $user->name.' has been Updated!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('admin.users.index')->with('error', 'User '.$user->name.' has been deleted!!!');
    }

    /**
     * Show reset password Form for user.
     *
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function showResetForm(User $user)
    {
        return view('admin.adminuser.reset-password', compact('user'));
    }

    /**
     * Resets the password of user.
     *
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function resetPassword(AdminUserPasswordRequest $request, User $user)
    {
        // $hashedPassword = $user->password;

        // PASSWORD RESET FUNCTION

        // if (Hash::check($request->old_password, $hashedPassword)) { 

            $user->password = Hash::make($request->password);
            $user->update(['password' => $user->password]);

            return redirect()->route('admin.users.index')->with('success', 'Password of '.$user->name.' has been reset successfully!!!');
        // }
    }

    // SEARCH FUNCTION FOR QUERYING
    public function search($keyword, $queryRole)
    {
        if (empty($keyword) && empty($queryRole))
        {
            $users = User::orderBy('id','ASC')->get();
        }
        elseif (empty($keyword)) {
            $users = User::whereHas('roles', function($q) use($queryRole){
                $q->whereId($queryRole);
            })->get();
        }
        elseif (empty($queryRole)) {
            $users = User::where('name', 'LIKE', "%$keyword%")->get();
        }
        elseif (!empty($keyword) && !empty($queryRole)) {
            $users = User::where('name', 'LIKE', "%$keyword%")
                ->WhereHas('roles', function($q) use($queryRole) {
                    $q->whereId($queryRole);
                })->get();
        }

        return $users;
    }
}
