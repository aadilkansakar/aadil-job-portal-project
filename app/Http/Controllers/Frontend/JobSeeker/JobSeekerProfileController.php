<?php

namespace App\Http\Controllers\Frontend\JobSeeker;

use App\Http\Controllers\Controller;
use App\Http\Requests\JobSeekerRequest;
use App\Models\JobCategory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class JobSeekerProfileController extends Controller
{
    public function jobseeker()
    {
        return Auth::guard('jobseeker')->user();
    }

    public function index()
    {   
        $job_categories = JobCategory::where('status', 1)->get();

        if ($this->jobseeker()->profile_picture && File::exists('images/jobseeker/'.$this->jobseeker()->profile_picture)) {
            $image = 'jobseeker/'.$this->jobseeker()->profile_picture;
        }
        else {
            $image = 'no-image.png';
        }

        return view('frontend.jobseekers.profile')->with([
            'jobseeker' => $this->jobseeker(),
            'image' => $image,
            'job_categories' => $job_categories,
        ]);
    }

    public function update(JobSeekerRequest $request)
    {
        // CHECK IF IMAGE HAS IMAGE BEFORE UPDATING

        if ($request->hasFile('profile_picture')) {
            if ($this->jobseeker()->profile_picture) {
                $destination = 'images/jobseeker/'.$this->jobseeker()->profile_picture;
                if (File::exists($destination)) {
                    unlink($destination);
                }
            }
            $file = $request->file('profile_picture');
            $extension = $file->getClientOriginalExtension();
            $fileName = time().'.'.$extension;
            $file->move('images/jobseeker/', $fileName);

            $this->jobseeker()->update($request->safe()->except(['profile_picture']) + ['profile_picture' => $fileName]);
        }
        else {
            $this->jobseeker()->update($request->validated());
        }

        return redirect()->route('jobseeker.profile')->with('success', 'Your Profile has been Updated!!');
    }

    public function destroyPhoto()
    {
        // IF FILE EXISTS IN DB AND PUBLIC FOLDER UNLINK

        if ($this->jobseeker()->profile_picture) {
            $destination = 'images/jobseeker/'.$this->jobseeker()->profile_picture;
            if (File::exists($destination)) {
                unlink($destination);
            }
        }

        $this->jobseeker()->update(['profile_picture' => NULL]);

        return redirect()->route('jobseeker.profile')->with('success', 'Your Photo has been deleted!!');
    }
}
