<?php

namespace App\Http\Controllers\Frontend\JobSeeker\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

class JobSeekerResetPasswordController extends Controller
{
    use ResetsPasswords;

    protected $redirectTo = 'jobseeker/profile';

    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware('guest:jobseeker');
        $this->middleware('guest:company');
    }

    public function showResetForm(Request $request, $token = null)
    {
        return view('frontend.jobseekers.auth.passwords.reset')
        ->with(['token' => $token, 'email' => $request->email]);
    }

    public function guard()
    {
        return Auth::guard('jobseeker');
    }

    protected function broker()
    {
        return Password::broker('jobseekers');
    }
}
