<?php

namespace App\Http\Controllers\Frontend\JobSeeker\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class JobSeekerForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware('guest:jobseeker');
        $this->middleware('guest:company');
    }

    public function showLinkRequestForm()
    {
        return view('frontend.jobseekers.auth.passwords.email');
    }

    protected function broker()
    {
        return Password::broker('jobseekers');
    }
}
