<?php

namespace App\Http\Controllers\Frontend\JobSeeker\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JobSeekerLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware('guest:jobseeker')->except('logout');
        $this->middleware('guest:company');
    }

    public function viewLogin()
    {
        return view('frontend.jobseekers.auth.login');
    }

    public function login(Request $request)
    {
        $this->validator($request);

        if (Auth::guard('jobseeker')->attempt($request->only('email', 'password') ,$request->filled('remember'))) {
            return redirect()->route('jobseeker.profile')->with('success', 'You are logged in!!');
        }

        return $this->loginFailed();
    }

    public function logout(Request $request)
    {
        Auth::guard('jobseeker')->logout();

        $request->session()->invalidate();
        
        return redirect()->route('index')->with('success', 'You have logged out');
    }

    public function validator(Request $request)
    {
        $rules = [
            'email' => 'required|email|exists:job_seekers|min:5|max:191',
            'password' => 'required|string|max:255',
        ];

        $messages = [
            'email|exists' => 'These credentials does not exist in our record',
        ];

        $request->validate($rules, $messages);
    }

    public function loginFailed()
    {
        return redirect()
        ->route('jobseeker.login.view')
        ->with('error', 'Login Failed!! Try Again Later');
    }
}
