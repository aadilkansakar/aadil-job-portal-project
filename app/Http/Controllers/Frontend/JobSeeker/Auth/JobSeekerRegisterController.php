<?php

namespace App\Http\Controllers\Frontend\JobSeeker\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\JobSeekerRequest;
use App\Models\JobSeeker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JobSeekerRegisterController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware('guest:jobseeker');
        $this->middleware('guest:company');
    }

    public function viewRegister()
    {
        return view('frontend.jobseekers.auth.register');
    }

    public function register(JobSeekerRequest $request)
    {
        $jobseeker = JobSeeker::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        $this->guard()->login($jobseeker);
        
        return redirect()->route('jobseeker.profile')->with('success', 'You have successfully Registered!!');
    }

    public function guard()
    {
        return Auth::guard('jobseeker');
    }
}
