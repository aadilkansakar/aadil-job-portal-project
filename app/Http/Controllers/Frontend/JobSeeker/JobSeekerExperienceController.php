<?php

namespace App\Http\Controllers\Frontend\JobSeeker;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class JobSeekerExperienceController extends Controller
{
    public function index()
    {
        return view('frontend.jobseekers.experience');
    }
}
