<?php

namespace App\Http\Controllers\Frontend\JobSeeker;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class JobSeekerEducationController extends Controller
{
    public function index()
    {
        return view('frontend.jobseekers.education');
    }
}
