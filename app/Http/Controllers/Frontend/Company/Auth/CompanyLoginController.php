<?php

namespace App\Http\Controllers\Frontend\Company\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompanyLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware('guest:company')->except('logout');
        $this->middleware('guest:jobseeker');
    }

    public function viewLogin()
    {
        return view('frontend.companies.auth.login');
    }

    public function login(Request $request)
    {
        $this->validator($request);

        if(Auth::guard('company')->attempt($request->only('email', 'password'), $request->filled('remember'))) {
            return redirect()
            ->intended(route('company.home'))
            ->with('success', 'You are logged in!!!');
        };

        return $this->loginFailed();
    }

    public function logout(Request $request)
    {
        Auth::guard('company')->logout();

        $request->session()->invalidate();

        return redirect()->route('index')->with('success', 'You have been logged out');
    }

    public function validator(Request $request)
    {
        $rules = [
            'email' => 'required|email|exists:companies|min:5|max:191',
            'password' => 'required|string|max:255',
        ];

        $messages = [
            'email.exists' => 'These credentials do not match our records.',
        ];

        $request->validate($rules,$messages);
    }

    public function loginFailed()
    {
        return redirect()
        ->back()
        ->withInput()
        ->with('error','Login failed, please try again!');
    }
}
