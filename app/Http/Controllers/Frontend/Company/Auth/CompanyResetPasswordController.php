<?php

namespace App\Http\Controllers\Frontend\Company\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

class CompanyResetPasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware('guest:jobseeker');
        $this->middleware('guest:company');
    }

    public function viewResetForm()
    {
        return view('frontend.companies.auth.reset-password');
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:4|confirmed',
        ]);

        $status = Password::broker('companies')->reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user, $password) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ])->setRememberToken(Str::random(60));

                $user->save();

                event(new PasswordReset(($user)));
            }
        );

        return $status === Password::PASSWORD_RESET
                    ? redirect()->route('company.login.view')->with(['status' => __($status)])
                    : back()->withErrors(['email' => __($status)]);
    }
}
