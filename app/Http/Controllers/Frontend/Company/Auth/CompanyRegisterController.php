<?php

namespace App\Http\Controllers\Frontend\Company\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyRequest;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompanyRegisterController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware('guest:company');
        $this->middleware('guest:jobseeker');
    }

    public function viewRegister()
    {
        return view('frontend.companies.auth.register');
    }

    public function register(CompanyRequest $request)
    {
        $company = Company::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        $this->guard()->login($company);

        return redirect()->route('company.home')->with('success', 'You have successfully Registered!!');
    }

    public function guard()
    {
        return Auth::guard('company');
    }
}
