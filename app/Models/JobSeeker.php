<?php

namespace App\Models;

use App\Notifications\JobSeekerResetPasswordNotification;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class JobSeeker extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $guard = 'jobseeker';

    protected $fillable = [
        'name', 
        'email', 
        'password', 
        'phone',
        'username',
        'date_of_birth', 
        'gender', 
        'profile_picture', 
        'bio',
        'job_category_id',
    ];

    protected $hidden = [
        'password',
        'token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new JobSeekerResetPasswordNotification($token));
    }
}
