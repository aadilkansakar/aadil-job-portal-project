<?php

namespace App\Models;

use App\Notifications\CompanyResetPasswordNotification;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Company extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $guard = 'company';

    protected $fillable = [
        'name',
        'industry_id',
        'email',
        'password',
        'phone',
        'description',
        'website',
        'logo',
    ];

    protected $hidden = [
        'password',
        'token',
    ];
}
