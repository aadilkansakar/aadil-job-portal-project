<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    use HasFactory;

    protected $fillable = [
        'job_seeker_id',
        'degree',
        'program',
        'institute_name',
        'graduation_year',
    ];
}
