@extends('layouts.app')

@section('content')
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4">Admin Roles</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.roles.index') }}">Roles</a></li>
                <li class="breadcrumb-item">{{ $role->name }}</li>
                <li class="breadcrumb-item active">Edit</li>
            </ol> 
            <div class="d-flex justify-content-center">
                <div class="card mb-4 w-50">
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        Edit Admin Roles
                    </div>
                    <div class="card-body">
                        <form action="{{ route('admin.roles.update', $role->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-floating mb-3">
                                <input class="form-control @error('name') is-invalid @enderror" id="inputname" type="text" placeholder="Name" name="name" value="{{ old('name') ?? $role->name }}"/>
                                <label for="inputname">Name</label>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class=" mb-3 ms-2 row">
                                <label for="inputname" class="mb-2">Permission</label>
                                @foreach ($permission as $value)
                                    <div class="form-check">
                                        <input class="form-check-input @error('permission') is-invalid @enderror" name="permission[]" type="checkbox" value="{{ $value->id }}" id="flexCheckDefault" {{ in_array($value->id, $rolePermissions) ? 'checked' : '' }}>
                                        <label class="form-check-label" for="flexCheckDefault">
                                        {{ $value->name }}
                                        </label>
                                    </div>
                                <br/>
                                @endforeach
                                @error('permission')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mt-4 mb-0 d-flex justify-content-center">
                                <div class="d-grid w-25"><button type="submit" class="btn btn-primary btn-block">Update</button></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
@endsection
