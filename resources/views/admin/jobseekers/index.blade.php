@extends('layouts.app')

@section('content')
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4">Job Seekers</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                <li class="breadcrumb-item active">Job Seekers</li>
            </ol>  
            @include('frontend.layouts.alert')
            <div class="mb-3">
                <form action="" class="row g-3">
                    <div class="col-sm-3">
                        <input type="search" name="keyword" class="form-control" placeholder="Search" value="{{ request()->query('keyword') }}">
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-primary me-1"><i class="fa fa-search me-1" aria-hidden="true"></i>Search</button>

                        <a href="{{ route('admin.jobseekers.index') }}">
                            <button type="button" class="btn btn-danger"><i class="fa-solid fa-backward me-1"></i></i>Reset</button>
                        </a>
                    </div>
                </form>
            </div>      
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table me-1"></i>
                    Job Seekers
                    @can('jobseeker-create')
                    <a class="btn btn-success float-end" href ="{{ route('admin.jobseekers.create') }}"><i class="fa-solid fa-plus"></i>
                        Create
                    </a>
                    @endcan
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="col-1">S.N</th>
                                <th class="col-2">Name</th>
                                <th class="col-2">Username</th>
                                <th class="col-3">Email</th>
                                <th class="col-4">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($jobseekers as $jobseeker)                                
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $jobseeker->name }}</td>
                                <td>{{ $jobseeker->username }}</td>
                                <td>{{ $jobseeker->email }}</td>
                                <td class="d-flex justify-content-start">
                                    @can('jobseeker-edit')
                                        <a class="btn btn-sm btn-warning me-1" href="{{ route('admin.jobseekers.edit', $jobseeker) }}"><i class="fa-solid fa-pen-to-square me-1"></i>Edit</a>
                                    @endcan
                                    
                                    @can('jobseeker-delete')
                                        <form action="{{ route('admin.jobseekers.destroy', $jobseeker) }}" method="POST" class="">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-sm btn-danger me-1"><i class="fa-solid fa-trash-can me-1"></i>Delete</button>
                                        </form>
                                    @endcan
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
</div>
@endsection
