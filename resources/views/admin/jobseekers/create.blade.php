@extends('layouts.app')

@section('content')
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4">Job Seekers</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.jobseekers.index') }}">Job Seekers</a></li>
                <li class="breadcrumb-item active">Create</li>
            </ol> 
            <div class="d-flex justify-content-center">  
                <div class="card mb-4 w-50">
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        Create Job Seekers
                    </div>
                    <div class="card-body">
                        <form action="{{ route('admin.jobseekers.store') }}" method="POST">
                            @csrf
                            <div class="form-floating mb-3">
                                <input class="form-control @error('name') is-invalid @enderror" id="name" type="text" placeholder="Name" name="name"/>
                                <label for="name">Name</label>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-floating mb-3">
                                <input class="form-control @error('phone') is-invalid @enderror" id="phone" type="text" placeholder="Phone" name="phone" />
                                <label for="phone">Phone</label>
                                @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-floating mb-3">
                                <input class="form-control @error('username') is-invalid @enderror" id="username" type="text" placeholder="Username" name="username" />
                                <label for="username">Username</label>
                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-floating mb-3">
                                <input class="form-control @error('email') is-invalid @enderror" id="email" type="text" placeholder="Email" name="email" />
                                <label for="email">Email</label>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-floating mb-3">
                                <input class="form-control @error('password') is-invalid @enderror" id="password" type="password" placeholder="Password" name="password" />
                                <label for="password">Password</label>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-floating mb-3">
                                <input class="form-control @error('password_confirmation') is-invalid @enderror" id="password_confirmation" type="password" placeholder="Password Again" name="password_confirmation" />
                                <label for="password_confirmation">Password Again</label>
                                @error('password_confirmation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="mt-4 mb-0 d-flex justify-content-center">
                                <div class="d-grid w-25"><button type="submit" class="btn btn-primary btn-block">Create</button></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
@endsection
