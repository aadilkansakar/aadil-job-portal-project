@extends('layouts.app')

@section('content')
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4">Job Categories</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.jobcategories.index') }}">Job Categories</a></li>
                <li class="breadcrumb-item active">Create</li>
            </ol> 
            <div class="d-flex justify-content-center">
                <div class="card mb-4 w-50">
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        Create Job Categories
                    </div>
                    <div class="card-body">
                        <form action="{{ route('admin.jobcategories.store') }}" method="POST">
                            @csrf
                            <div class="form-floating mb-3">
                                <input class="form-control @error('name') is-invalid @enderror" id="name" type="text" placeholder="Name" name="name" value="{{ old('name') }}"/>
                                <label for="name">Name</label>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-floating mb-3">
                                <input class="form-control @error('slug') is-invalid @enderror" id="slug" type="text" placeholder="Slug" name="slug" value="{{ old('slug') }}" />
                                <label for="slug">Slug</label>
                                @error('slug')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-floating mb-3 ms-2 row">
                                <div class="form-check col-2">
                                    <input class="form-check-input @error('status') is-invalid @enderror" type="radio" name="status" id="flexRadioDefault1" value="1" {{ (old('status') == "1") ? 'checked' : '' }}>
                                    <label class="form-check-label" for="flexRadioDefault1">
                                    Active
                                    </label>
                                </div>
                                <div class="form-check col-2">
                                    <input class="form-check-input @error('status') is-invalid @enderror" type="radio" name="status" id="flexRadioDefault2" value="0" {{ (old('status') == "0") ? 'checked' : '' }}>
                                    <label class="form-check-label" for="flexRadioDefault2">
                                    Inactive
                                    </label>
                                </div>
                                @error('status')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mt-4 mb-0 d-flex justify-content-center">
                                <div class="d-grid w-25"><button type="submit" class="btn btn-primary btn-block">Create</button></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<script>
    $('#name').change(function(e) {
      $.get('{{ route('admin.jobcategory.checkSlug') }}', 
        { 'name': $(this).val() }, 
        function( data ) {
          $('#slug').val(data.slug);
        }
      );
    });
</script>
@endsection
