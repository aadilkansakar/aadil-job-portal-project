@extends('layouts.app')

@section('content')
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4">Job Categories</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                <li class="breadcrumb-item active">Job Categories</li>
            </ol>  
            @include('frontend.layouts.alert')
            <div class="mb-3">
                <form action="" class="row g-3">
                    <div class="col-sm-3">
                        <input type="search" name="keyword" class="form-control" placeholder="Search" value="{{ request()->query('keyword') }}">
                    </div>
                    <div class="col-sm-2">
                        <select type="search" name="status" class="form-control" placeholder="Search" aria-label="Search">
                            <option value="all">All</option>
                            <option value=1 {{ request()->query('status') == '1' ? 'selected' : '' }}>Active</option>
                            <option value=0 {{ request()->query('status') == '0' ? 'selected' : '' }}>Inactive</option>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-primary me-1"><i class="fa fa-search me-1" aria-hidden="true"></i>Search</button>

                        <a href="{{ route('admin.jobcategories.index') }}">
                            <button type="button" class="btn btn-danger"><i class="fa-solid fa-backward me-1"></i></i>Reset</button>
                        </a>
                    </div>
                </form>
            </div>      
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table me-1"></i>
                    Job Categories
                    @can('jobcategory-create')
                    <a class="btn btn-success float-end" href ="{{ route('admin.jobcategories.create') }}"><i class="fa-solid fa-plus"></i>
                        Create
                    </a>
                    @endcan
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="col-1">S.N</th>
                                <th class="col-3">Name</th>
                                <th class="col-3">Slug</th>
                                <th class="col-2">Status</th>
                                <th class="col-3">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($jobcategories as $jobcategory)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $jobcategory->name }}</td>
                                <td>{{ $jobcategory->slug }}</td>
                                <td>
                                    <label class="badge {{ ($jobcategory->status) == 1 ? 'bg-success' : 'bg-danger' }}">
                                        {{ ($jobcategory->status) == 1 ? 'Active' : 'Inactive' }}
                                    </label>
                                </td>
                                <td class="d-flex justify-content-start">
                                    @can('jobcategory-edit')
                                        <a class="btn btn-sm btn-warning me-1" href="{{ route('admin.jobcategories.edit', $jobcategory) }}"><i class="fa-solid fa-pen-to-square me-1"></i>Edit</a>
                                    @endcan
                                    
                                    @can('jobcategory-delete')
                                        <form action="{{ route('admin.jobcategories.destroy', $jobcategory) }}" method="POST" class="">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-sm btn-danger me-1"><i class="fa-solid fa-trash-can me-1"></i>Delete</button>
                                        </form>
                                    @endcan
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
</div>
@endsection
