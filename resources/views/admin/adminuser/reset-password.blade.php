@extends('layouts.app')

@section('content')
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4">Admin Users</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.users.index') }}">Users</a></li>
                <li class="breadcrumb-item">{{ $user->name }}</li>
                <li class="breadcrumb-item active">Reset Password</li>
            </ol>
            <div class="d-flex justify-content-center">
                <div class="card mb-4 w-50 ">
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        Admin Reset Password
                    </div>
                    <div class="card-body">
                        <form action="{{ route('admin.users.password.reset', $user->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            {{-- <div class="form-floating mb-3">
                                <input class="form-control @error('old_password') is-invalid @enderror" id="inputold_password" type="password" placeholder="old_password" name="old_password">
                                <label for="inputold_password">Old Password</label>
                                @error('old_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div> --}}
                            <div class="form-floating mb-3">
                                <input class="form-control @error('password') is-invalid @enderror" id="inputpassword" type="password" placeholder="password" name="password">
                                <label for="inputpassword">New Password</label>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-floating mb-3">
                                <input class="form-control @error('password') is-invalid @enderror" id="inputpassword" type="password" placeholder="password" name="password_confirmation">
                                <label for="inputpassword">New Password Again</label>
                                @error('password_confirmation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="mt-4 mb-0 d-flex justify-content-center">
                                <div class="d-grid w-25"><button type="submit" class="btn btn-primary btn-block">Reset Password</button></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
@endsection
