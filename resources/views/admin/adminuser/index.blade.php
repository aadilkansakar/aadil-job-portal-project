@extends('layouts.app')

@section('content')
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4">Admin Users</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                <li class="breadcrumb-item active">Users</li>
            </ol>  
            @include('frontend.layouts.alert')
            <div class="mb-4">
                <form action="" class="row g-3">
                    <div class="col-sm-3">
                        <input type="search" name="keyword" class="form-control" placeholder="Search" aria-label="Search" value="{{ request()->query('keyword') }}">
                    </div>
                    <div class="col-sm-2">
                        <select type="search" name="role" class="form-control" placeholder="Search" aria-label="Search">
                            <option value="{{ NULL }}">Select Role</option>
                            @foreach ($allRoles as $role)
                                <option value="{{ $role->id }}" {{ request()->query('role') == $role->id ? 'selected' : '' }}>{{ $role->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-primary me-1"><i class="fa fa-search me-1" aria-hidden="true"></i>Search</button>

                        <a href="{{ route('admin.users.index') }}">
                            <button type="button" class="btn btn-danger"><i class="fa-solid fa-backward me-1"></i></i>Reset</button>
                        </a>
                    </div>
                </form>
            </div>
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table me-1"></i>
                    Admin Users
                    @can('user-create')
                    <a class="btn btn-success float-end" href ="{{ route('admin.users.create') }}"><i class="fa-solid fa-plus"></i>
                        Add User
                    </a>
                    @endcan
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="col-1">S.N.</th>
                                <th class="col-2">Name</th>
                                <th class="col-2">Email</th>
                                <th class="col-2">Role</th>
                                <th class="col-3">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($users as $user)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    <label class="badge bg-secondary">{{ $user->getRoleNames()->first() }}</label>
                                </td>
                                <td class="d-flex justify-content-start">
                                    @if(!$user->hasRole(['SuperAdmin']))
                                        @can('user-edit')
                                            <a class="btn btn-sm btn-warning me-1" href="{{ route('admin.users.edit', $user->id) }}"><i class="fa-solid fa-pen-to-square me-1"></i>Edit</a>
                                        @endcan
                                        @can('user-delete')
                                            <form action="{{ route('admin.users.destroy', $user->id) }}" method="POST" class="">
                                            @csrf
                                            @method('DELETE')
                                                <button type="submit" class="btn btn-sm btn-danger me-1"><i class="fa-solid fa-trash-can me-1"></i>Delete</button>
                                            </form>      
                                        @endcan
                                        @can('user-password-reset')
                                            <a class="btn btn-sm btn-success" href="{{ route('admin.users.password.show', $user->id) }}"><i class="fa-solid fa-arrows-rotate me-1"></i>Reset Password</a>
                                        @endcan
                                    @endif
                                </td>
                            </tr>
                            @empty
                                <tr>
                                    <td></td>
                                    <td>No Results Found</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
</div>
@endsection
