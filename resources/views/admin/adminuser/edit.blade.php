@extends('layouts.app')

@section('content')
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4">Admin Users</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.users.index') }}">Users</a></li>
                <li class="breadcrumb-item">{{ $user->name }}</li>
                <li class="breadcrumb-item active">Edit</li>
            </ol>
            <div class="d-flex justify-content-center">
                <div class="card mb-4 w-50 ">
                    <div class="card-header">
                        <i class="fas fa-table me-1"></i>
                        Edit Admin Users
                    </div>
                    <div class="card-body">
                        <form action="{{ route('admin.users.update', $user->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-floating mb-3">
                                <input class="form-control @error('name') is-invalid @enderror" id="inputname" type="text" placeholder="Name" name="name" value="{{ old('name') ?? $user->name }}"/>
                                <label for="inputname">Name</label>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-floating mb-3">
                                <input class="form-control @error('username') is-invalid @enderror" id="inputusername" type="text" placeholder="username" name="username" value="{{ old('username') ?? $user->username }}" />
                                <label for="inputusername">Username</label>
                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-floating mb-3">
                                <input class="form-control @error('email') is-invalid @enderror" id="inputemail" type="email" placeholder="email" name="email" value="{{ old('email') ?? $user->email }}" />
                                <label for="inputemail">Email</label>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-floating mb-3">
                                <input class="form-control @error('phone') is-invalid @enderror" id="inputphone" type="text" placeholder="phone" name="phone" value="{{ old('phone') ?? $user->phone }}" />
                                <label for="inputphone">Phone</label>
                                @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-floating mb-3">
                                <select class="form-control @error('roles') is-invalid @enderror" id="inputrole" type="role" placeholder="role" name="roles">
                                <label for="inputrole">Role</label>
                                <option disabled selected>Select Role</option>
                                
                                @foreach ($roles as $role)
                                    @if (Auth::user()->hasRole(1))
                                        @if (!empty($userRole)) 
                                            <option value="{{ $role->name }}" @if (old('roles') || $userRole->name == $role->name) selected @endif>{{ $role->name }}</option>
                                        @else
                                            <option value="{{ $role->name }}" @if(old('roles')) selected @endif>{{ $role->name }}</option>
                                        @endif
                                    @else
                                        @if ($role->id != 1)
                                            @if (!empty($userRole))
                                                <option value="{{ $role->name }}" @if (old('roles') || $userRole->name == $role->name) selected @endif>{{ $role->name }}</option>
                                            @else 
                                                <option value="{{ $role->name }}" @if (old('roles')) selected @endif>{{ $role->name }}</option>
                                            @endif
                                        @endif
                                    @endif
                                @endforeach
                                @error('roles')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror  
                                
                                </select>
                            </div>
                            <div class="mt-4 mb-0 d-flex justify-content-center">
                                <div class="d-grid w-25"><button type="submit" class="btn btn-primary btn-block">Update</button></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
@endsection
