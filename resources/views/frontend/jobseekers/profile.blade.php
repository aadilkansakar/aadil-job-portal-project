@extends('frontend.layouts.dash-app')

@section('title', 'Job Seeker Dashboard')

@section('content')

<!-- Content Start -->
    
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Profile</h1>
                
                @include('frontend.layouts.alert')

                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Start Page Content -->
                    <!-- ============================================================== -->
                    <!-- Row -->
                    <div class="row">
                      <!-- Column -->
                      <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                          <div class="card-body">
                            <center class="m-t-30">
                              <img
                                src="{{ asset('images/'.$image) }}"
                                class="rounded-circle"
                                width="150"
                              />
                              <h4 class="card-title m-t-10">{{ $jobseeker->name }}</h4>
                              <h6 class="card-subtitle">{{ $jobseeker->username }}</h6>
                              <div class="row text-center justify-content-md-center mt-3">
                                <form action="{{ route('jobseeker.profile.picture.delete') }}" method="post">
                                @csrf
                                @method('PUT')
                                <button class="btn btn-sm btn-danger">Remove Photo</button>
                                </form>
                              </div>
                            </center>
                          </div>
                          <div>
                            <hr />
                          </div>
                          <div class="card-body">
                            <small class="text-muted">Email address </small>
                            <h6>{{ $jobseeker->email }}</h6>
                            <small class="text-muted p-t-30 db">Phone</small>
                            <h6>{{ $jobseeker->phone }}</h6>
                            <small class="text-muted p-t-30 db">Date of Birth</small>
                            <h6>{{ $jobseeker->date_of_birth }}</h6>
                            <small class="text-muted p-t-30 db">Gender</small>
                            <h6>{{ $jobseeker->gender }}</h6>
                            <small class="text-muted p-t-30 db">Bio</small>
                            <h6>{{ $jobseeker->bio }}</h6>
                          </div>
                        </div>
                      </div>
                      <!-- Column -->
                      <!-- Column -->
                      <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                          <div class="card-body">
                            <form action="{{ route('jobseeker.profile.update') }}" method="POST" class="form-horizontal form-material mx-2" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                              <div class="form-group">
                                <label class="col-md-12">Full Name</label>
                                <div class="col-md-12">
                                  <input
                                    name="name"
                                    type="text"
                                    placeholder="Johnathan Doe"
                                    class="form-control form-control-line"
                                    value="{{ old('name') ?? $jobseeker->name }}"
                                  />
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="example-email" class="col-md-12">Email</label>
                                <div class="col-md-12">
                                  <input
                                    name="email"
                                    type="email"
                                    placeholder="johnathan@admin.com"
                                    class="form-control form-control-line"
                                    id="example-email"
                                    value="{{ old('email') ?? $jobseeker->email }}"
                                  />
                                </div>
                                @error('email')
                                    <span class="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                              </div>
                              <div class="form-group">
                                <label class="col-md-12">Phone No</label>
                                <div class="col-md-12">
                                  <input
                                    name="phone"
                                    type="text"
                                    placeholder="123 456 7890"
                                    class="form-control form-control-line"
                                    value="{{ old('phone') ?? $jobseeker->phone }}"
                                  />
                                </div>
                                @error('phone')
                                    <span class="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                              </div>
                              <div class="form-group">
                                <label class="col-md-12">Date of Birth</label>
                                <div class="col-md-12">
                                    <input
                                    name="date_of_birth"
                                    type="date"
                                    placeholder="123 456 7890"
                                    class="form-control form-control-line"
                                    value="{{ old('date_of_birth') ?? $jobseeker->date_of_birth }}"
                                  />
                                </div>
                                @error('date_of_birth')
                                    <span class="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                              </div>
                              <div class="form-group">
                                <label class="col-sm-12">Select Gender</label>
                                <div class="col-sm-12">
                                  <select
                                    class="form-select shadow-none form-control-line"
                                    name="gender"
                                  >
                                    <option value="">---</option>
                                    <option value="Male" {{ ($jobseeker->gender) == 'Male' ? 'selected' : '' }}>Male</option>
                                    <option value="Female" {{ ($jobseeker->gender) == 'Female' ? 'selected' : '' }}>Female</option>
                                    <option value="Others" {{ ($jobseeker->gender) == 'Others' ? 'selected' : '' }}>Others</option>
                                  </select>
                                </div>
                                @error('gender')
                                    <span class="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                              </div>
                              <div class="form-group row">
                                <label class="col-md-12">Profile Picture</label>
                                <div class="col-md-12">
                                    <input
                                    name="profile_picture"
                                    type="file"
                                    placeholder="123 456 7890"
                                    class="form-control form-control-line"
                                  />
                                </div>
                                @error('profile_picture')
                                    <span class="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                              </div>
                              <div class="form-group">
                                <label class="col-md-12">Bio</label>
                                <div class="col-md-12">
                                  <textarea
                                    name="bio"
                                    rows="5"
                                    class="form-control form-control-line"
                                  >{{ old('bio') ?? $jobseeker->bio }}</textarea>
                                </div>
                                @error('bio')
                                    <span class="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                              </div>
                              <div class="form-group">
                                <label class="col-sm-12">Select Job Category</label>
                                <div class="col-sm-12">
                                  <select
                                    class="form-select shadow-none form-control-line"
                                    name="job_category_id"
                                  >
                                    <option value="{{ NULL }}">------</option>
                                    @foreach ($job_categories as $category)
                                      <option value="{{ $category->id }}" {{ $category->id == Auth::user()->job_category_id ? 'selected' : ''}}>{{ $category->name }}</option>
                                    @endforeach
                                  </select>
                                </div>
                                @error('job_category_id')
                                    <span class="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                              </div>
                              <div class="form-group">
                                <div class="col-sm-12 mt-2">
                                  <button class="btn btn-success text-white">
                                    Update Profile
                                  </button>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                      <!-- Column -->
                    </div>
                    <!-- Row -->
                    <!-- ============================================================== -->
                    <!-- End PAge Content -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Right sidebar -->
                    <!-- ============================================================== -->
                    <!-- .right-sidebar -->
                    <!-- ============================================================== -->
                    <!-- End Right sidebar -->
                    <!-- ============================================================== -->
                  </div>
                  <!-- ============================================================== -->
                  <!-- End Container fluid  -->
                  <!-- ============================================================== -->
            </div>
        </main>
    </div>

@endsection