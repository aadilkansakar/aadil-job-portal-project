<!-- Navbar Start -->
<nav class="navbar navbar-expand-lg bg-white navbar-light shadow sticky-top p-0">
    <a href="{{ route('index') }}" class="navbar-brand d-flex align-items-center text-center py-0 px-4 px-lg-5">
        <h1 class="m-0 text-primary">JobEntry</h1>
    </a>
    <button type="button" class="navbar-toggler me-4" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <div class="navbar-nav ms-auto p-4 p-lg-0">
            <a href="{{ route('index') }}" class="nav-item nav-link">Home</a>
            <a href="about.html" class="nav-item nav-link">About</a>
            <div class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Jobs</a>
                <div class="dropdown-menu rounded-0 m-0">
                    <a href="job-list.html" class="dropdown-item">Job List</a>
                    <a href="job-detail.html" class="dropdown-item">Job Detail</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Pages</a>
                <div class="dropdown-menu rounded-0 m-0">
                    <a href="category.html" class="dropdown-item">Job Category</a>
                    <a href="testimonial.html" class="dropdown-item">Testimonial</a>
                    <a href="404.html" class="dropdown-item">404</a>
                </div>
            </div>
            <a href="contact.html" class="nav-item nav-link">Contact</a>
        </div>
        @if (Auth::guard('jobseeker')->check())    
            <div class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">
                    {{ Auth::guard('jobseeker')->user()->name }}<span class="caret"></span>
                </a>
                <div class="dropdown-menu rounded-0 m-0">
                    <a href="{{ route('jobseeker.profile') }}" class="dropdown-item">My Profile</a>
                    <form action="{{ route('jobseeker.logout') }}" method="POST">
                    @csrf
                    <button class="dropdown-item">Logout</button>
                    </form>
                </div>
            </div>
        @elseif (Auth::guard('company')->check())    
            <div class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">
                    {{ Auth::guard('company')->user()->name }}<span class="caret"></span>
                </a>
                <div class="dropdown-menu rounded-0 m-0">
                    <a href="{{ route('company.home') }}" class="dropdown-item">Dashboard</a>
                    <form action="{{ route('company.logout') }}" method="POST">
                    @csrf
                    <button class="dropdown-item">Logout</button>
                    </form>
                </div>
            </div>
        @elseif (Auth::check())
            <div class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">
                    {{ Auth::user()->name }}<span class="caret"></span>
                </a>
                <div class="dropdown-menu rounded-0 m-0">
                    <a href="{{ route('admin.home') }}" class="dropdown-item">Admin Panel</a>
                    <form action="{{ route('logout') }}" method="POST">
                    @csrf
                    <button class="dropdown-item">Logout</button>
                    </form>
                </div>
            </div>
        @else
            <div class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Job Seeker</a>
                <div class="dropdown-menu rounded-0 m-0">
                    <a href="{{ route('jobseeker.login.view') }}" class="dropdown-item">Login</a>
                    <a href="{{ route('jobseeker.register.view') }}" class="dropdown-item">Register</a>
                </div>
            </div>

            <div class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Company</a>
                <div class="dropdown-menu rounded-0 m-0">
                    <a href="{{ route('company.login.view') }}" class="dropdown-item">Login</a>
                    <a href="{{ route('company.register.view') }}" class="dropdown-item">Register</a>
                </div>
            </div>
            <a href="{{ route('login') }}" class="nav-item nav-link">Admin</a>
        @endif        
    </div>
</nav>
<!-- Navbar End -->