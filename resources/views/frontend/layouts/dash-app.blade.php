<!DOCTYPE html>
<html lang="en">

@include('frontend.layouts.header')

<body>
    <div class="container-xxl bg-white p-0">
        <!-- Spinner Start -->
        <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <!-- Spinner End -->

        @include('frontend.layouts.navbar')
        
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Dashboard</div>
                            <a class="nav-link {{ (request()->segment(2) == 'profile') ? 'active' : '' }}" href="{{ route('jobseeker.profile') }}">
                                <div class="sb-nav-link-icon"><i class="fa-solid fa-user"></i></div>
                                Profile
                            </a>
                            <a class="nav-link {{ (request()->segment(2) == 'education') ? 'active' : '' }}" href="{{ route('jobseeker.education') }}">
                                <div class="sb-nav-link-icon"><i class="fa-solid fa-graduation-cap"></i></div>
                                Education
                            </a>
                            <a class="nav-link {{ (request()->segment(2) == 'experience') ? 'active' : '' }}" href="{{ route('jobseeker.experience') }}">
                                <div class="sb-nav-link-icon"><i class="fa-solid fa-briefcase"></i></div>
                                Experience
                            </a>
                            <a class="nav-link {{ (request()->segment(2) == 'resume') ? 'active' : '' }}" href="{{ route('jobseeker.resume') }}">
                                <div class="sb-nav-link-icon"><i class="fa-solid fa-file"></i></div>
                                Resume
                            </a>
                        </div>
                    </div>
                </nav>
            </div>
            @yield('content')
        </div>        

        @include('frontend.layouts.footer')
    </div>  
</body>

</html>