@extends('frontend.layouts.app')

@section('title', 'Company Dashboard')

@section('content')

<!-- Header Start -->
<div class="container-xxl py-5 bg-dark page-header mb-5">
    <div class="container my-5 pt-5 pb-4">
        <h1 class="display-3 text-white mb-3 animated slideInDown">Company Dashboard</h1>
    </div>
</div>
<!-- Header End -->

@endsection