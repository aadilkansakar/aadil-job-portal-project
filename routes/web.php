<?php

use App\Http\Controllers\Admin\AdminRoleController;
use App\Http\Controllers\Admin\AdminUserController;
use App\Http\Controllers\Admin\CompanyIndustryController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\JobCategoryController;
use App\Http\Controllers\Admin\JobSeekerController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::middleware('auth')->prefix('admin')->name('admin.')->group(function() {
    Route::get('/home', [HomeController::class, 'index'])->name('home');
    
    Route::resource('users', AdminUserController::class);
    Route::get('users/{user}/reset-password', [AdminUserController::class, 'showResetForm'])->name('users.password.show');
    Route::put('users/{user}/resets', [AdminUserController::class, 'resetPassword'])->name('users.password.reset');

    Route::resource('roles', AdminRoleController::class);

    Route::resource('jobcategories', JobCategoryController::class);
    Route::get('jobcategories/create/checkSlug', [JobCategoryController::class, 'checkSlug'])->name('jobcategory.checkSlug');

    Route::resource('jobseekers', JobSeekerController::class);
    
    Route::resource('companyindustries', CompanyIndustryController::class);
});

include('frontend.php');
