<?php

use App\Http\Controllers\Frontend\Company\Auth\CompanyForgotPasswordController;
use App\Http\Controllers\Frontend\Company\Auth\CompanyLoginController;
use App\Http\Controllers\Frontend\Company\Auth\CompanyRegisterController;
use App\Http\Controllers\Frontend\Company\Auth\CompanyResetPasswordController;
use App\Http\Controllers\Frontend\Company\CompanyHomeController;
use App\Http\Controllers\Frontend\JobSeeker\Auth\JobSeekerForgotPasswordController;
use App\Http\Controllers\Frontend\JobSeeker\Auth\JobSeekerLoginController;
use App\Http\Controllers\Frontend\JobSeeker\Auth\JobSeekerRegisterController;
use App\Http\Controllers\Frontend\JobSeeker\Auth\JobSeekerResetPasswordController;
use App\Http\Controllers\Frontend\JobSeeker\JobSeekerEducationController;
use App\Http\Controllers\Frontend\JobSeeker\JobSeekerExperienceController;
use App\Http\Controllers\Frontend\JobSeeker\JobSeekerHomeController;
use App\Http\Controllers\Frontend\JobSeeker\JobSeekerProfileController;
use App\Http\Controllers\Frontend\JobSeeker\JobSeekerResumeController;
use Illuminate\Support\Facades\Route;

    Route::get('/', function () {
        return view('frontend.index');
    })->name('index');

    Route::prefix('jobseeker')->name('jobseeker.')->group(function() {
        Route::get('register', [JobSeekerRegisterController::class, 'viewRegister'])->name('register.view');
        Route::post('register', [JobSeekerRegisterController::class, 'register'])->name('register');
        
        Route::get('login', [JobSeekerLoginController::class, 'viewlogin'])->name('login.view');
        Route::post('login', [JobSeekerLoginController::class, 'login'])->name('login');

        Route::post('logout', [JobSeekerLoginController::class, 'logout'])->name('logout');

        Route::post('password/email', [JobSeekerForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');
        Route::get('password/reset', [JobSeekerForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');

        Route::post('password/reset', [JobSeekerResetPasswordController::class, 'reset']);
        Route::get('password/reset/{token}', [JobSeekerResetPasswordController::class, 'showResetForm'])->name('password.reset');

        Route::middleware('auth:jobseeker')->group(function() {
            Route::get('home', [JobSeekerHomeController::class, 'index'])->name('home');

            Route::get('profile', [JobSeekerProfileController::class, 'index'])->name('profile');
            Route::put('profile/update', [JobSeekerProfileController::class, 'update'])->name('profile.update');
            Route::put('profile/picture/delete', [JobSeekerProfileController::class, 'destroyPhoto'])->name('profile.picture.delete');

            Route::get('education', [JobSeekerEducationController::class, 'index'])->name('education');

            Route::get('experience', [JobSeekerExperienceController::class, 'index'])->name('experience');

            Route::get('resume', [JobSeekerResumeController::class, 'index'])->name('resume');
        });
    });

    Route::prefix('company')->name('company.')->group(function() {
        Route::namespace('Auth')->group(function() {
            Route::get('register', [CompanyRegisterController::class, 'viewRegister'])->name('register.view');
            Route::post('register', [CompanyRegisterController::class, 'register'])->name('register');
    
            Route::get('login', [CompanyLoginController::class, 'viewlogin'])->name('login.view');
            Route::post('login', [CompanyLoginController::class, 'login'])->name('login');

            Route::post('logout', [CompanyLoginController::class, 'logout'])->name('logout');

            Route::get('password/reset', [CompanyForgotPasswordController::class, 'viewRequestForm'])->name('password.request');
            Route::post('password/email', [CompanyForgotPasswordController::class, 'sendResetLink'])->name('password.email');

            Route::get('password/reset/{token}', [CompanyResetPasswordController::class, 'viewResetForm'])->name('password.reset');
            Route::post('password/reset', [CompanyResetPasswordController::class, 'updatePassword'])->name('password.update');
        });
        
        Route::middleware('auth:company')->group(function() {
            Route::get('home', [CompanyHomeController::class, 'index'])->name('home');
        });
    });

?>