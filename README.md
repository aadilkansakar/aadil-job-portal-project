## About This Project

This is a Job Portal website made using Laravel framework.
In this project, you can login to the CMS and Jobseekers Profile pages

## Read before installing

- Make a new database named 'job-portal'
- Make changes to .env file after generation if needed.

## Installing this project

- cd project_folder
- composer install
- cp .env.example .env
- php artisan key:generate
- php artisan migrate:fresh --seed
- php artisan serve

## Email and Password for CMS Users

- SuperAdmin : aadilkansakar@gmail.com | password
- Administrator : samina@example.com | password
- Manager : ekbana@example.com | password
- Viewer : jamesmilner@example.com | password

## Register and Login JobSeekers in Frontend